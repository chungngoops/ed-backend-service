## Setting 

Setup local environment.

1. Install [**AWS CLI version 2**](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) and set your credentials in your local machine.
2. Install **NodeJs** and **NPM**.
3. Change directory to **ed-backend-service**
4. Run `npm install`

---

## Creating Tables and Setting up Policies 


1. Change your directory to **Setup**.
2. Setup Dynamodb endpoint:
   - edit file **createPatientsTable.js** and **loadDataToPatientsTable.js** by set `region` and `endpoint` value in this code block to your correct one.
   ```
   AWS.config.update({
   region: "ap-southeast-1",
   endpoint: "dynamodb.ap-southeast-1.amazonaws.com"});
   ```
2. Create the table **Patients** by running : `node createPatientsTable.js`.
3. Load sample data to **Patients** table by running: `node loadDataToPatientsTable.js`
4. Create AWS Role and Policy to grant Lambda access to Dynamodb:
   - Follow instructions in this [link](https://aws.amazon.com/blogs/security/how-to-create-an-aws-iam-policy-to-grant-aws-lambda-access-to-an-amazon-dynamodb-table/).
   - Find the policy template in the file: `PatientLambdaPolicy.json`.
   - **NOTE:** don't forget to set `Resource` to your correct setting.

Before you move on,repeat above steps to create **HeartRates** table, load data and create policy to allow Lambda access.

---

## Create Lambda functions 

Use these steps to  create **getAllPatientsLambdaFunction** Lambda function.

1. Login to your **AWS Console** and find service **Lambda** 
2. Create a new function by clicking on **Creat function**
3. On the Creat function wizard:
   - Choose option: `Author from scratch`
   - Function name: give the name to your function, ex: `getAllPatientsLambdaFunction`.
   - Choose **Runtime**: `Nodejs 14.x`
   - On the **Permissions** section, change the **Execution role** to **Use an existing role** and then select the role you created.
   - Click on `Create function` in the bottom right of the wizard.
4. Change the function execution code
   - Change directory to **LambdaFunction**
   - Copy the code in `getAllPatientsLambdaFunction.js`
   - Paste the code to `index.js` in the new function's `Code` section.

Repeat the process to create other Lambda functions.

---
## Setup API Gateway

Setup API Gateway to invoke Lambda function.

1. Sign in to the API Gateway console at https://console.aws.amazon.com/apigateway.
2. Do one of the following:
   - To create your first API, for HTTP API, choose Build.
   - If you've created an API before, choose Create API, and then choose Build for HTTP API.
3. Create a new route: 
    - On the **Routes** section.
    - Choose **Create** to create a new route.
    - Choose **Method** `GET` and enter **path** `patients` 
4. Attach Lambda function to the created route:
    - On the **Integrations** section.
    - Select the route created in the 5th step: `/patiens`.
    - Click on `Create and attach an integration`
    - On **Integration target** choose **Integration type**: `Lambda function`
    - On **Integration details**:
        - Select your **AWS Region**
        - Search and select the function you want to attach in the **Lambda function** search box. In this case is : `getAllPatientsLambdaFunction`
        - Choose **Create**.
5. Click on **Deploy** button on the top right of the screen.
6. After the deployment process finish you can navigate to the API Details section to see your API's **Invoke URL**.

Repeat these steps to create and attach function to other routes:
- GET `API_Invoke_URL/patients/{patient_id}` with function `LambdaFunction/getPatientDetailsLambdaFunction.js`
- GET `API_Invoke_URL//patients/{patient_id}/heartbeats??startDate={start_date}&endDate={end_date}` with function `LambdaFunction/getHeartRatesLambdaFunction.js`

