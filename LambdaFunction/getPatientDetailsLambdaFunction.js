const AWS = require('aws-sdk');

const dynamo = new AWS.DynamoDB();

exports.handler = async (event, context) => {

    let body;
    let statusCode = '200';
    const headers = {
        'Content-Type': 'application/json',
    };

    let params = {
        Key: {
            "id": {
                S: event.pathParameters.patient_id
            }
        },
        TableName: "Patients"
    }
    try {

        let item = await dynamo.getItem(params).promise();
        if (!item.Item) {
            statusCode = '404';
            body = 'Patient not found';
        } else {
            body = item.Item.data;
        }

    } catch (err) {
        statusCode = '400';
        body = err.message;
    } finally {
        body = JSON.stringify(body);
    }

    return {
        statusCode,
        body,
        headers,
    };
};
