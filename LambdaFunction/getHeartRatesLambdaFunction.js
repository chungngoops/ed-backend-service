const AWS = require('aws-sdk');

const docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event, context) => {
    let body;
    let statusCode = '200';
    const headers = {
        'Content-Type': 'application/json',
    };
    let params;
    if (event.queryStringParameters) {
        if (event.queryStringParameters.startDate && event.queryStringParameters.endDate) {
            params = {
                TableName: "HeartRates",
                KeyConditionExpression: "#id = :id and #recorded_date BETWEEN :startDate AND :endDate",
                ExpressionAttributeNames: {
                    "#id": "id",
                    "#recorded_date": "recorded_date"
                },
                ExpressionAttributeValues: {
                    ":id": event.pathParameters.patient_id,
                    ":startDate": event.queryStringParameters.startDate,
                    ":endDate": event.queryStringParameters.endDate
                }
            }
        } else {
            statusCode = '400';
            body = "Invalid param";

            return {
                statusCode,
                body,
                headers,
            };
        }
    } else {
        params = {
            TableName: "HeartRates",
            KeyConditionExpression: "#id = :id ",
            ExpressionAttributeNames: {
                "#id": "id",
            },
            ExpressionAttributeValues: {
                ":id": event.pathParameters.patient_id,
            }
        }
    }
    try {
        let data = await docClient.query(params).promise()
        if (!data.Items || data.Items.length == 0) {
            statusCode = '404';
            body = "Patient not found"
        } else {
            console.log(data);
            body = data.Items;
        }

    } catch (err) {
        statusCode = '400';
        body = err.message;
    } finally {
        body = JSON.stringify(body);
    }

    return {
        statusCode,
        body,
        headers,
    };
}